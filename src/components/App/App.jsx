import React from 'react';
import {
  BrowserRouter,
  Route,
  Switch,
  Link,
} from 'react-router-dom';

import Home from '../Home';
import About from '../About';
import Posts from '../Posts';

const App = () => (
  <BrowserRouter>
    <Link to="/home">Home</Link>
    <Link to="/about">About</Link>
    <Link to="/posts">Posts</Link>

    <Switch>
      <Route
        exact
        path="/home"
        component={Home}
      />

      <Route
        exact
        path="/about"
        component={About}
      />

      <Route
        path="/posts"
        component={Posts}
      />
    </Switch>
  </BrowserRouter>
);

export default App;
