import React from 'react';
import PropTypes from 'prop-types';

const Post = (props) => {
  const posts = [
    {
      id: 1,
      title: 'Getting started with Redux',
      author: 'Syed Hammad Ahmed',
    },

    {
      id: 2,
      title: 'React Hooks',
      author: 'Stephen Grider',
    },
  ];

  const { match } = props;
  const post = posts.filter((p) => p.id === parseInt(match.params.postId, 10))[0];

  return (
    post
      ? (
        <div style={{ margin: '5px', marginTop: '10px' }}>
          <h3>{post.title}</h3>
          <small>{post.author}</small>
        </div>
      )
      : (
        <div style={{ margin: '5px', marginTop: '10px' }}>
          <h3>Not Found</h3>
        </div>
      )
  );
};

Post.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      postId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default Post;
