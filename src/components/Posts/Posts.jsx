import React from 'react';
import { Route, Link } from 'react-router-dom';

import Post from '../Post';

const Posts = () => {
  const posts = [
    {
      id: 1,
      title: 'Getting started with Redux',
      author: 'Syed Hammad Ahmed',
    },

    {
      id: 2,
      title: 'React Hooks',
      author: 'Stephen Grider',
    },
  ];

  return (
    <div style={{ marginTop: '10px' }}>
      {
        posts.map((post) => (
          <div key={post.id}>
            <Link to={`/posts/${post.id}`}>{post.title}</Link>
          </div>
        ))
      }

      <Route
        exact
        path="/posts/:postId"
        component={Post}
      />
    </div>
  );
};

export default Posts;
